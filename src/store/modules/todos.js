import axios from 'axios';


const state = {
  todos: []
};

const getters = {
  allTodos: (state) => state.todos
};

const actions = {
  async fetchTodos({ commit }) {
    const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
    
    commit('setTodos', response.data);
  },
  async addTodo({ commit }, title) {
    const response = await axios.post('https://jsonplaceholder.typicode.com/todos', {title, completed: false});
    
    commit('newTodo', response.data);
  },
  async deleteTodo({ commit }, id) {
    await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);

    commit('removeTodo', id);
  },
  async filterTodos({ commit }, value) {
    //Get Selected number from options
    //const limit = e;
    
    
    const limit = parseInt(value);
    const response = await axios.get(`https://jsonplaceholder.typicode.com/todos/?_limit=${limit}`);
    commit('limitTodos', response.data);
  },
  async updateTodo({ commit }, upTodo) {
    await axios.get(`https://jsonplaceholder.typicode.com/todos/${upTodo.id}`);

    commit('markTodo', upTodo)
  }
};

const mutations = {
  setTodos: (state, todos) => {
    state.todos = todos;
  },
  newTodo: (state, todo) => {
    state.todos.unshift(todo);
  },
  removeTodo: (state, id) => {
    state.todos = state.todos.filter(todo => todo.id !== id);
  },
  limitTodos: (state, todos) => {
    state.todos = todos;
  },
  markTodo: (state, upTodo) => {
    const index = state.todos.findIndex(todo => todo.id === upTodo.id); 
    if(index != -1) {
      state.todos.splice(index, 1, upTodo);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}